
Random Forest (2D)

Description: 
	This program computes a Random Forest classifier (RForest) to perform
	classification of two different classes (positive and negative) in a 2D
	feature space (x1,x2). The RForest computes multiple random binary trees
	using information gain and decision stumps (axis-aligned features) at 
	every tree node.

	For further information, please refer to [1].

Comments:
	The parameters of the classifier and the 2D feature scenario can be
	found in the fun_parameters_2d.m function.

	Two different sets of samples are used in this program. The first one 
	-trnSamples- is used to train the classifier, whereas the second one 
	-tstSamples- is used to evaluate the generalization capabilities of the
	classifier.

	Three different scenarios to train and test the classifier have been
	considered, each one with a particular degree of complexity.

Steps:
	Steps to execute the program:
	1. Run the prg_setup.m file to configure the program paths.
	2. Run the prg_random_forest_2d.m file to compute the classifier and to
	   perform classification on the 2D problem scenario. 

References:
	[1] Criminisi, Antonio, Jamie Shotton, and Ender Konukoglu. "Decision
	    forests: A unified framework for classification, regression, 
	    density estimation, manifold learning and semi-supervised learning."
	    Foundations and Trends® in Computer Graphics and Vision 7, no. 2–3
     	    (2012): 81-227.

Contact:
	Michael Villamizar
	mvillami-at-iri.upc.edu
	Institut de Robòtica i Informática Industrial CSIC-UPC
	Barcelona - Spain
	2016
