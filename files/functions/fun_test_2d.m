%%-----------------------------------------------------------------------------
% Test step
% This function tests the classifier on the input samples in order to 
% measure its classification performance. 
function output = fun_test_2d(clfr,samples)
if (nargin~=2), fun_messages('Incorrect input variables','error'); end

% variables
tic;  % time
labels = samples(:,3);  % sample labels
numSamples = size(samples,1);  % num. samples

% allocate
scores = zeros(1,numSamples);  % samples scores

% Sample scores: the classifier is tested on the input samples to compute
% the sample scores or classifier confidence over these samples.
for iterSample = 1:numSamples
    scores(1,iterSample) = fun_classifier_test(clfr,samples(iterSample,1:2));
end

% messages
fun_messages(sprintf('Test time per sample: %.4f [msec]', 1000*toc...
	/numSamples),'information');
 
% Classification rates: this function computes some classification 
% rates of the classifier over the input samples.
clfRates = fun_classification_rates(scores,labels);

% classification results
results.eer = clfRates.eer;  % equal error rate (EER)
results.thr = clfRates.thr;  % classification threshold to obain EER
results.idx = clfRates.idx;  % threshold index -EER index-
results.scores = scores;  % classification scores
results.curves.rec = clfRates.rec;  % recall curve
results.curves.pre = clfRates.pre;  % precision curve
results.curves.fme = clfRates.fme;  % f-measure curve
results.curves.tps = clfRates.tps;  % true positives curve
results.curves.fps = clfRates.fps;  % false positives curve
results.curves.fns = clfRates.fns;  % false negatives curve

% output
output = results;
end
%%-----------------------------------------------------------------------------
