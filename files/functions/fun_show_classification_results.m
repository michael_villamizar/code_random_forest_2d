%%-----------------------------------------------------------------------------
% Show classification results
% This function shows the classification results on the input samples.
function fun_show_classification_results(results,clfr,samples,text)
if (nargin~=4), fun_messages('Incorrect inputs','error'); end

% parameters
prms = fun_parameters_2d();  % program parameters
fs = prms.visualization.fontSize;  % visualization: font size
lw = prms.visualization.lineWidth;  % visualization: line width
ms = prms.visualization.markerSize;  % visualization: marker size
pc = prms.visualization.posColor;  % visualization: positive color
nc = prms.visualization.negColor;  % visualization: negative color

% results
eer = results.eer;  % equal error rate
thr = results.thr;  % classification threshold
ind = results.idx;  % threshold index
rec = results.curves.rec;  % recall curve
pre = results.curves.pre;  % precision curve
fme = results.curves.fme;  % f-measure curve
tps = results.curves.tps;  % true positives curve
fps = results.curves.fps;  % false positives curve        
fns = results.curves.fns;  % false negatives curve
scores = results.scores;  % classification scores

% variables
labels = samples(:,3);  % sample labels
numSamples = size(samples,1);  % num. samples

%% messages
fun_messages(sprintf('Results: %s',text),'process');
fun_messages(sprintf('Equal Error Rate: %.3f',eer),'information');
fun_messages(sprintf('Recall: %.3f',rec(ind)),'information');
fun_messages(sprintf('Precision: %.3f',pre(ind)),'information');
fun_messages(sprintf('True positives: %d',tps(ind)),'information');
fun_messages(sprintf('False positives: %d',fps(ind)),'information');
fun_messages(sprintf('False negatives: %d',fns(ind)),'information');
fun_messages(sprintf('Threshold: %.3f',thr),'information');

% show precision-recall plot
figure,subplot(221),plot(rec,pre,'k-','linewidth',lw),grid on,hold on;
title('Precision-Recall Plot','fontsize',fs);
xlabel('Recall','fontsize',fs),ylabel('Precision','fontsize',fs);
legend(sprintf('EER: %.3f',eer));

% show classification results
subplot(222),axis([0,1,0,1]),hold on,grid on

% samples
for iterSample = 1:numSamples
    
    % sample properties
    lbl = labels(iterSample);   % true label
    scr = scores(iterSample);   % score
    est = sign(2*(scr>thr)-1);  % estimated label
    
    % plot sample
    if (lbl>0)
        if (lbl==est)
            % true positive
            plot(samples(iterSample,2),samples(iterSample,1),'+','color',pc,...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false negative
            plot(samples(iterSample,2),samples(iterSample,1),'+','color',...
	    	[0,0,0],'markersize',ms,'linewidth',lw),hold on
        end
    else
        if (lbl==est)
            % true negative
            plot(samples(iterSample,2),samples(iterSample,1),'o','color',nc,...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false positive
            plot(samples(iterSample,2),samples(iterSample,1),'o','color',...
	    	[0,0,0],'markersize',ms,'linewidth',lw),hold on
        end
    end
end

% messages
title('Classification Results','fontsize',fs), hold on;
legend('Positive','Negative');
xlabel('x1','fontsize',fs), ylabel('x2','fontsize',fs)

% variable
entThr = 0.9;   % entropy threshold

% compute classification and uncertainty maps
[clfMap,uncMap] = fun_classification_maps_2d(clfr);

% show classification map
subplot(223),imagesc(clfMap,[0,1]), hold on;
xlabel('Sample Space','fontsize',fs);
title('Classification Map','fontsize',fs), hold on;
xlabel('x1','fontsize',fs), ylabel('x2','fontsize',fs)

% show uncertainty map
subplot(224),imagesc(uncMap,[0,1]), hold on;
contour(rgb2gray(uncMap),entThr,'linewidth',lw), hold on;
xlabel(sprintf('Entropy boundary -> %.2f',entThr),'fontsize',fs);
title('Uncertainty Map','fontsize',fs), hold on;
xlabel('x1','fontsize',fs), ylabel('x2','fontsize',fs)

end
%%-----------------------------------------------------------------------------
