%%-----------------------------------------------------------------------------
%% Classifier: Random Forest classifier (RForest)
% This function computes the random forest classifier (RForest) using
% decision stumps in the 2d feature space. These stumps are computed using
% the information gain over a random set of decision splits.
function output = fun_classifier_rforest_2d(samples)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% message
fun_messages('Classifier: Random Forest (RForest)','process');

% parameters
prms = fun_parameters_2d();  % program parameters
phi = prms.forest.phi;  % sampling factor
numTrees = prms.forest.numTrees;  % num. random trees

% num. samples
numSamples = size(samples,1);

% sample set size: random subset
setSize = round(phi*numSamples);

% variables
tic;  % time

% random trees
for iter = 1:numTrees

    % messages
    if (mod(iter,5)==0),fun_messages(sprintf('Tree: %d/%d',iter,...
	    numTrees),'information'); end
    
    % random sample indexes
    indxs = randperm(numSamples);
    
    % random sample subset
    subset = samples(indxs(1:setSize),:);
    
    % create root node
    root = fun_node(subset,0);
    
    % tree growing
    tree = fun_grow(root);

    % ensemble classifier
    clfr.tree(iter) = tree;
end

% messages
fun_messages(sprintf('Training time: %.4f [sec]',toc),'information');

% output
output = clfr;
end
%%-----------------------------------------------------------------------------


%%-----------------------------------------------------------------------------
% Tree node
% This function computes a new node in the random decision tree. 
function output = fun_node(samples,depth)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% sample labels
labels = samples(:,3)';

% class distribution
hstm = hist(labels,-1:2:1) + eps;
hstm = hstm./sum(hstm(:));

% entropy
ent = -1*sum(hstm.*log2(hstm));

% create node
node.ent = ent;  % entropy
node.gain = 0;  % information gain
node.hstm = hstm;  % class distribution
node.depth = depth + 1;  % node depth
node.theta = 0;  % threshold [0,1]
node.alpha = 0;  % 2d space feature {x1,x2}
node.samples = samples;  % node samples
node.leftNode = [];  % left child node 
node.rightNode = [];  % right child node
node.numSamples = size(samples,1);  % num. samples
node.leftSamples = []; % left child node samples
node.rightSamples = [];  % right child node samples
node.numLeftSamples = 0;  % num. left child node samples
node.numRightSamples = 0;  % num. right child node samples

% output
output = node;
end
%%-----------------------------------------------------------------------------


%%-----------------------------------------------------------------------------
% Tree growing
% This function grows the tree node using the information gain.
function node = fun_grow(node)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters_2d();  % program parameters
kappa = prms.forest.kappa;  % num. random features
maxDepth = prms.forest.maxDepth;  % max. tree depth
minSamples = prms.forest.minSamples;  % min. num. node samples 

% check depth and num. sample in the node
if (node.depth==maxDepth || node.numSamples<=minSamples), return; end

% variables
maxGain = 0;

% random features
for iter = 1:kappa
    
	% random feature -decision stump-
	theta = rand;  % threshold [0,1]
	alpha = min(2,ceil(rand*2));  % 2D space feature {x1,x2}

	% right and left sample indexes
	leftIndxs = find(node.samples(:,alpha)<=theta); 
	rightIndxs = find(node.samples(:,alpha)>theta);  
         
	% left and right samples
	leftSamples = node.samples(leftIndxs,:);
	rightSamples = node.samples(rightIndxs,:);
   
	% num. left and right samples
	numLeftSamples = size(leftIndxs,1);
	numRightSamples = size(rightIndxs,1);
      
	% class distributions
	leftHstm = hist(leftSamples(:,3),-1:2:1) + eps;
	rightHstm = hist(rightSamples(:,3),-1:2:1) + eps;
   
	% normalization
	leftHstm = leftHstm./sum(leftHstm(:));
	rightHstm = rightHstm./sum(rightHstm(:));
   
	% entropy
	leftEnt = -1*sum(leftHstm.*log2(leftHstm),2);
	rightEnt = -1*sum(rightHstm.*log2(rightHstm),2);
   
	% information gain
	gain = node.ent - (numLeftSamples*leftEnt+numRightSamples*rightEnt)...
		/node.numSamples;
   
	% max. information gain
	if (gain>maxGain)
       
		% update values
		maxGain = gain;

		% update node
		node.gain = gain;  % information gain
		node.alpha = alpha;  % 2d space feature
		node.theta = theta;  % feature threshold
		node.leftSamples = leftSamples;  % left child node samples
		node.rightSamples = rightSamples;  % right child node samples
		node.numLeftSamples = numLeftSamples;  % num. left child node samples
		node.numRightSamples = numRightSamples;  % num. right child node samples
	end
end

% check max. information gain
if (maxGain==0), return; end

% child nodes
node.leftNode = fun_node(node.leftSamples,node.depth);
node.rightNode = fun_node(node.rightSamples,node.depth);

% recursive growing
node.leftNode = fun_grow(node.leftNode);
node.rightNode = fun_grow(node.rightNode);
end
%%-----------------------------------------------------------------------------
