%%-----------------------------------------------------------------------------
% Classification rates
% This function computes some classification rates of the classifier over 
% samples. The function uses the sample scores -classifier confidence- with
% the sample labels to compute these classification rates. Specifically, 
% the function computes the precision, recall and f-measure plots, and the
% equal error rate (EER), that is the point where recall and precision are
% equal. The function also computes true positives, false positives and
% false negatives rates.
function output = fun_classification_rates(scores,labels)
if (nargin~=2), fun_messages('Incorrect input variables','error'); end

% positive and negative indexes
posIndxs = find(labels==+1);
negIndxs = find(labels==-1);

% variables
eer = 0;  % equal error rate
mrt = inf;  % max. rate
thr = 0;  % classification threshold
cnt = 0;  % counter 
idx = 0;  % index
num = 1000;  % num. iterations

% allocate
rec = zeros(1,num);  % recall
pre = zeros(1,num);  % precision
fme = zeros(1,num);  % f-measure
tps = zeros(1,num);  % true positives
fps = zeros(1,num);  % false positives
fns = zeros(1,num);  % false negatives

% min. and max. threshold values
maxThr = max(scores);  % max. threshold
minThr = min(scores);  % min. threshold

% threshold step
stepThr = (maxThr-minThr)/(num-1);

% threshold
for iterThr = maxThr:-stepThr:minThr
   
    % counter
    cnt = cnt + 1;
    
    % rates
    tp = sum(scores(posIndxs)>iterThr,2);   % true positives
    fp = sum(scores(negIndxs)>iterThr,2);   % false positives
    fn = sum(scores(posIndxs)<=iterThr,2);  % false negatives
    
    % classification rates
    r = tp/(tp + fn);  % recall
    p = tp/(tp + fp);  % precision
    f = 2*r*p/(r+p);  % f-measure
    
    % dif. recall-precision rate
    rate = abs(r-p);
    
    % best rate
    if (rate<mrt && r~=0 && p~=0)
        % update values
        mrt = rate;  % max. rate
        thr = iterThr;  % threshold
        eer = (r+p)/2;  % equal error rate
        idx = cnt;  % index
    end
    
    % save values
    tps(cnt) = tp;
    fps(cnt) = fp;
    fns(cnt) = fn;
    rec(cnt) = r;
    pre(cnt) = p;
    fme(cnt) = f;
    
end

% check
if (eer==0), fun_messages('EER value is zero','warning'); end

% classification data
data.eer = eer;  % equal error rate (EER)
data.thr = thr;  % classification threshold to achieve EER
data.idx = idx;  % EER rate index
data.rec = rec;  % recall rate
data.pre = pre;  % precision rate
data.fme = fme;  % f-measure rate
data.tps = tps;  % true positive
data.fps = fps;  % false positive
data.fns = fns;  % false negative

% output
output = data;
end
%%-----------------------------------------------------------------------------
