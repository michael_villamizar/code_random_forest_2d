%%-----------------------------------------------------------------------------
% Test the classifier
% This function tests the classifier on the input sample. The function
% returns the classifier confidence or score over this sample.
function output = fun_classifier_test(clfr,sample)
if (nargin~=2), fun_messages('Incorrect input variables','error'); end

% num. random binary trees
numTrees = size(clfr.tree,2);

% variables
score = zeros(1,numTrees);

% test random trees
for iterTree = 1:numTrees
    % test classier and update sample score
    score(iterTree) = fun_test_node(clfr.tree(iterTree),sample);
end

% average score
score = mean(score);

% output 
output = score;
end
%%-----------------------------------------------------------------------------


%%-----------------------------------------------------------------------------
% Test node feature
% This function test the node on the input sample.
function score = fun_test_node(node,sample)
if (nargin~=2), fun_messages('Incorrect input variables','error'); end

% positive class score
score = node.hstm(2);

% check terminal -leaf- node
if (node.theta==0 && node.alpha==0), return; end

% feature value
value = sample(1,node.alpha);

% test feature
if (value>node.theta)
    % right child node
    score = fun_test_node(node.rightNode,sample);
else
    % left child node
    score = fun_test_node(node.leftNode,sample);
end

end
%%-----------------------------------------------------------------------------
