%%-----------------------------------------------------------------------------
% Show samples
% This function shows the samples -positive and negative- in the 2D space.
function fun_show_samples_2d(samples,text)
if (nargin~=2), fun_messages('Incorrect input variables','error'); end

% parameters
prms = fun_parameters_2d();  % program parameters
ms = prms.visualization.markerSize;  % visualization: marker size
lw = prms.visualization.lineWidth;  % visualization: line width
fs = prms.visualization.fontSize;  % visualization: font size
pc = prms.visualization.posColor;  % visualization: positive color
nc = prms.visualization.negColor;  % visualization: negative color

% positive and negative sample indexes
posIndxs = find(samples(:,3)==+1);
negIndxs = find(samples(:,3)==-1);

% figure
figure,plot(samples(posIndxs,2),samples(posIndxs,1),'+','color',pc,...
	'markersize',ms,'linewidth',lw), hold on
plot(samples(negIndxs,2),samples(negIndxs,1),'o','color',nc,'markersize',...
	ms,'linewidth',lw), hold on
axis([0,1,0,1]), grid on
title(sprintf('%s Samples',text),'fontsize',fs);
legend('Positives','Negatives');
xlabel('x1', 'fontSize',fs), ylabel('x2', 'fontSize',fs)

end
%%-----------------------------------------------------------------------------
