%%-----------------------------------------------------------------------------
% Parameters
% This function returns the program parameters.
function output = fun_parameters_2d()

% random forest
phi = 0.3;  % sampling factor
kappa = 50;  % num. random features
maxDepth = 9;  % max. tree  depth
numTrees = 30;  % num. random trees
minSamples = 10;  % min. num. samples
forest = struct('kappa',kappa,'phi',phi,'maxDepth',maxDepth,'numTrees',...
	numTrees,'minSamples',minSamples);

% samples
example = 3;  % scenario example {1,2,3}
numPosSamples = 500;  % num. positive samples
numNegSamples = 500;  % num. negative samples
samples = struct('example',example,'numPosSamples',numPosSamples,...
	'numNegSamples',numNegSamples);

% visualization
fontSize = 10;  % font width
posColor = [0,0.8,0.8];  % color for positive samples
negColor = [0.8,0,0];  % color for negative samples
gridSize = 100;  % grid size
lineWidth = 4;  % line width
markerSize = 16;  % marker size
visualization = struct('markerSize',markerSize,'lineWidth',lineWidth,...
	'fontSize',fontSize,'gridSize',gridSize,'posColor',posColor,...
	'negColor',negColor);

% parameters
prms.forest = forest;  % random forest classifier parameters
prms.samples = samples;  % samples parameters
prms.visualization = visualization; % visualization parameters

% output
output = prms;
end
%%-----------------------------------------------------------------------------
